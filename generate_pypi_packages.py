"""
 Create PyPI packages for OSX and LINUX (source).
"""

import os
import sys
from shutil import which
import platform



##### MAIN #####
def main() -> None:

    minchia  = 0

    # check OS
    OS = platform.system()
    isDarwin = True
    if not OS == 'Darwin':
        sys.stderr.write("ERROR: you must generate the packages on a MacOS system.")
        sys.exit(-5)

    # check python installation
    pythonPath = which("python3")
    if pythonPath is None:
        sys.stderr.write("ERROR: the command \"python3\" was not found. Please install python3.")
        sys.exit(-5)

    from sh import python3
    # set log paths
    # get path to the script
    pySrcDir = os.path.dirname(os.path.abspath(__file__))
    setupPath = os.path.join(pySrcDir, "setup.py")
    # check the extans of the setup file
    if not  os.path.isfile(setupPath):
        sys.stderr.write("ERROR: the setup file \n{:s}\n was not found in the directory\n{:s}\n".format(setupPath, pySrcDir))
        sys.exit(-2)
    # generate the sdist package
    stdoutPath: str = os.path.join(pySrcDir, "generate_sdist.stdout.txt")
    stderrPath: str = os.path.join(pySrcDir, "generate_sdist.stderr.txt")
    python3(setupPath, "sdist", _out=stdoutPath, _err=stderrPath)

    # generate the bdist (OSX) package
    stdoutPath: str = os.path.join(pySrcDir, "generate_bdist.stdout.txt")
    stderrPath: str = os.path.join(pySrcDir, "generate_bdist.stderr.txt")
    python3(setupPath, "bdist_wheel", _out=stdoutPath, _err=stderrPath)

    # print some info on how to upload the packages
    print("PyPI packages generated successfully, in order to upload to PyPI run the following command:")
    print("twine upload --sign </dist/distribution_file_name>")
    print("\nor to upload a test version, execute the following:")
    print("twine upload --repository-url https://test.pypi.org/legacy/ --sign </dist/distribution_file_name>")

    print("\nTo install a package from the test PyPI repository use the following command:")
    print("pip3 install --index-url https://test.pypi.org/simple/ <package_name>")

if __name__ == "__main__":
    main()
